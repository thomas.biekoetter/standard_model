from StandardModel import Higgs
from StandardModel.util.alphaS import alphaS_atQ

xs = Higgs.XS()
print("sigma_ggH(mh=125, LHC-13TeV) = " + str(xs.get_ggh(125., 13)))

br = Higgs.BR()
print("Br_h_bb(mh=125) = " + str(br.get_bb(125.)))
print("Br_h_yy(mh=125) = " + str(br.get_yy(125.)))
print("Gam_tot(mh=125) = " + str(br.get_Gam_tot(125.)))

print("alphaS(Q=MZ) = " + str(alphaS_atQ(91.)[0]))
