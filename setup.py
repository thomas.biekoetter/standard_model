from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="StandardModel",
    version="0.1.0",
    description="Package that provides SM observables.",
    long_description=long_description,
    packages=["StandardModel"],
    author=["Thomas Biekotter"],
    author_email=["thomas.biekoetter@desy.de"],
    url="https://gitlab.com/thomas.biekoetter/standard_model",
    include_package_data=True,
    package_data={"StandardModel": ["*"]},
    install_requires=["numpy", "scipy", "pandas"],
    python_requires='>=3.6',
)
