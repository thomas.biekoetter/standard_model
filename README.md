# StandardModel

The only purpose of this package is to obtain the values
of Standard Model observables.

Each particle shall have its own module, and different
kind of observables shall be implemented as a class
within the module.
So far, implemented are the LHC 13TeV production cross sections
and the branching ratios of the Higgs boson of the
Standard Model.
I encourage anyone to contribute in case she/he thinks
this package can be useful for her/him.

## Installation

Do:
```
python -m pip install --user -U .
```
## Usage

See the example scripts in the test folder for how
to use this package.

## Citation guide

The Standard Model predictions you have access to here
were calculated in the following references. If you
use these numbers in a scientific publication,
please be so kind and cite the corresponding papers.

### Higgs boson

LHC cross sections:
[Handbook of LHC Higgs Cross Sections: 3. Higgs Properties](https://inspirehep.net/literature/1241571)

Branching ratios:
[Handbook of LHC Higgs Cross Sections: 4. Deciphering the Nature of the Higgs Sector](https://inspirehep.net/literature/1494411)
